using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyGarage.Service.Abstraction;

namespace MyGarage.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase

    {
        private ICarsService _carsService;

        public CarsController(ICarsService service)
        {
            _carsService = service;
            
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _carsService.Delete(id);
            return Ok();
        }
        
    }
    
    
    
    
}