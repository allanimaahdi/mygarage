using Microsoft.AspNetCore.Identity;

namespace MyGarage.Data.Abstraction.Models
{
    public class ApplicationUser : IdentityUser
    {
        
    }
}