using System;
using System.ComponentModel.DataAnnotations;

namespace MyGarage.Data.Abstraction.Models
{
    public class Car
    {
        [Key]
        public int ID {get; set;}
        [Required]
        public string Brand { get; set; }
        public string Model { get; set; }
        [Required]
        public int CreationYear {get; set;}
        [Required]
        [Range(1, 7)]
        public int NbSeats {get; set;}
        public int Power {get; set;}
        public int Price {get; set;}

        
    }
}