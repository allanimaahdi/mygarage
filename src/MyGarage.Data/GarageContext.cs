using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MyGarage.Data.Abstraction.Models;

namespace MyGarage.Data
{
    public class GarageContext: IdentityDbContext<ApplicationUser>

    {


        public GarageContext(DbContextOptions options) : base(options)
        {
            
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            ApplicationUser admin = new ApplicationUser
            {
                Id = Guid.NewGuid().ToString(),
                AccessFailedCount = 0,
                LockoutEnabled = true,
                UserName = "Allani",
                Email = "exemple@gmail.com",
                EmailConfirmed = true,
                NormalizedEmail = "EXEMPLE@GMAIL.COM",
                NormalizedUserName = "EXEMPLE",
                SecurityStamp = "ce907",
                ConcurrencyStamp = "32fe9448",
            };
            var passwordHasher = new PasswordHasher<ApplicationUser>();
            string passwordHash = passwordHasher.HashPassword(admin, "Admin123");
            admin.PasswordHash = passwordHash;
            modelBuilder.Entity<ApplicationUser>().HasData(admin);
        }
        public DbSet<Car> Cars { get; set; }
        
    }
}