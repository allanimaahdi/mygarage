﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyGarage.Data.Migrations
{
    public partial class AddSeededCar : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Brand",
                table: "Cars",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "ID", "Brand", "CreationYear", "Model", "NbSeats", "Power", "Price" },
                values: new object[] { 2, "Ford", 1964, "Mustang", 3, 224, 180000 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.AlterColumn<string>(
                name: "Brand",
                table: "Cars",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
