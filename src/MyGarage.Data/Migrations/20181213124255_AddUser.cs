﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyGarage.Data.Migrations
{
    public partial class AddUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "de1ed400-28e4-421f-9164-0121c666b2cf", 0, "32fe9448", "exemple@gmail.com", true, true, null, "EXEMPLE@GMAIL.COM", "EXEMPLE", "AQAAAAEAACcQAAAAEPgxWBvEHFKgqzuhLmIwjkVH/X9VHa2PtQ1rJWnlCkeoAzZxJYIPXDhUTUpjKRUPDw==", null, false, "ce907", false, "Allani" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "de1ed400-28e4-421f-9164-0121c666b2cf", "32fe9448" });
        }
    }
}
