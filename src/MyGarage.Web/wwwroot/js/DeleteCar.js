$(".delete-car").click(
    function () {
        var row = $(this).parent().parent();
        var carId = $(this).data("car-id");
        $.ajax({
            url:"/api/cars/"+carId,
            type: "DELETE",
            success: function(){
                row.remove();
            }
        })
        
    });
