using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyGarage.Data;
using MyGarage.Data.Abstraction.Models;

namespace MyGarage.Web.ViewComponents
{
    public class CarViewComponent : ViewComponent
    {
        private readonly GarageContext  _context;
        
        public CarViewComponent(GarageContext context)
        {
            _context = context;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = _context.Cars.ToList();
            return await Task.FromResult((IViewComponentResult) View("Index", model));
            
        }
        
    }
}