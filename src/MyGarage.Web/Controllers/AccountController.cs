using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyGarage.Data.Abstraction.Models;
using MyGarage.Web.ViewModels.Account;

namespace MyGarage.Web.Controllers
{
    public class AccountController : Controller
    {


        private SignInManager<ApplicationUser> _signInManager;

        public AccountController(SignInManager<ApplicationUser> manager)
        {
            _signInManager = manager;
        }
        
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([Bind("Username,Password")] LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, false, false);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid Login");
                    return View(model);
                }
            }

            return View(model);
        }

        public IActionResult AccessDenied()
        {
            return View();

        }
        
    }
}