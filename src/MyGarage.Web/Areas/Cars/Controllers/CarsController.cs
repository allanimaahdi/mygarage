using Microsoft.AspNetCore.Mvc;
using MyGarage.Data;
using MyGarage.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyGarage.Service.Abstraction;

namespace MyGarage.Web.Areas.Cars.Controllers
{    
    [Area("Cars")]
    public class CarsController : Controller
    {
        public async Task <IActionResult> Index()
        {
            IEnumerable<Car> Cars = await _carsService.GetAll();
            return View(Cars);
        }
        private readonly ICarsService _carsService;
        
        public CarsController(ICarsService service)
        {
            _carsService = service;
        }

        
        
        // CAR CREATION \\
        
        public IActionResult Create()
        {
            return View();
        }        
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Brand,Model,CreationYear,NbSeats,Power,Price")] Car car)
        {
            if (ModelState.IsValid)
            {
                await _carsService.Add(car);
                return RedirectToAction(nameof(Index));
            }
            return View(car);
        }
        
        // Car Edition  \\
        
       
        
        // GET: Movies/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var car = await _carsService.GetById((int)id);
            if (car == null)
            {
                return NotFound();
            }
            return View(car);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Brand,Model,CreationYear,NbSeats,Power,Price")] Car car)
        {
            if (id != car.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
              
                  await  _carsService.Update(id, car);
                    
                
                return RedirectToAction("Index");
            }
            return View(car);
        }
        
        
        
    }
    
    
}