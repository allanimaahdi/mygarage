using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyGarage.Data.Abstraction.Models;

namespace MyGarage.Service.Abstraction
{
    public interface ICarsService
    {
        Task Add(Car car);
        Task Delete(int id);
        Task<IEnumerable<Car>> GetAll();

        Task<Car> GetById(int id);

        Task Update(int id, Car car);
    }
}