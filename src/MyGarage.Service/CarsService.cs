using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using MyGarage.Data;
using MyGarage.Data.Abstraction.Models;
using MyGarage.Service.Abstraction;
using Microsoft.Extensions.Logging;
namespace MyGarage.Service
{
    public class CarsService : ICarsService
    {
        private ILogger _logger;
        private GarageContext _context;
        private IMemoryCache _cache;
        public CarsService(GarageContext context, IMemoryCache cache, ILogger<CarsService> logger)
        {
            _context = context;
            _cache = cache;
            _logger = logger;
        }
        public async Task Add(Car car)
        {
            _cache.Remove("CarsList");
            _context.Add(car);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {    _cache.Remove("CarsList");
            Car car = await GetById(id);
            _context.Remove(car);
            await _context.SaveChangesAsync();
        }
        public async Task<IEnumerable<Car>> GetAll()
        {
            IEnumerable<Car> Cars;
            if (!_cache.TryGetValue("CarsList", out Cars))
            {
                _logger.LogWarning("Getting cars list from Database");
                Cars = await _context.Cars.ToListAsync();
                _cache.Set("CarsList", Cars);
            }
            else
            {
                _logger.LogWarning("Getting Cars from cache");
            }
            return Cars;
        }

        public async Task<Car> GetById(int id)
        {
            return await _context.Cars.FindAsync(id);
        }

        public async Task Update(int id, Car car)
        {
            _cache.Remove("CarsList");
            _context.Update(car);
            await _context.SaveChangesAsync();
        }
    }
}